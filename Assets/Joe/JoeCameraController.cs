﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoeCameraController : MonoBehaviour {

    GameObject joe;
	// Use this for initialization
	void Start () {
        this.joe = GameObject.Find("Joe");
        
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(this.joe.transform.position.x, this.joe.transform.position.y+1, this.joe.transform.position.z);

    }
}
