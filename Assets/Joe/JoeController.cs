﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoeController : MonoBehaviour
{
    private Animator anim;
    private Rigidbody rigid;
    //前に歩く速さ
    private float ForwordForce = 75.0f;
    //後ろにステップする速さ
    private float BackForce = 75.0f;
    //左右にステップする速さ
    private float SideForce = 75.0f;
    //走る速さ
    private float RunForwordForce = 75.0f;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //前に歩くとき
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            anim.SetBool("is_walking", true);
            rigid.AddForce(this.transform.forward * ForwordForce);
        }
        if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.W))
        {
            anim.SetBool("is_walking", false);
        }
        //走るとき
        if ((Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W)) || (Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.UpArrow)))
        {
            anim.SetBool("is_runnning", true);
            rigid.AddForce(this.transform.forward * RunForwordForce);
        }
        //シフトのみで走り解除
        if ((Input.GetKeyUp(KeyCode.LeftShift) && Input.GetKey(KeyCode.W)) || (Input.GetKeyUp(KeyCode.RightShift) && Input.GetKey(KeyCode.UpArrow)))
        {
            anim.SetBool("is_runnning", false);
            anim.SetBool("is_walking", true);
            rigid.AddForce(this.transform.forward * ForwordForce);
        }
        //普通に解除
        if ((Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.W)) || (Input.GetKeyUp(KeyCode.RightShift) || Input.GetKeyUp(KeyCode.UpArrow)))
        {
            anim.SetBool("is_runnning", false);
        }
        //後ろに歩くとき
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            anim.SetBool("is_backwalking", true);
            rigid.AddForce(-this.transform.forward * BackForce);
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S))
        {
            anim.SetBool("is_backwalking", false);
        }
        //右連続を防ぐ
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("RightStep"))
        {
            anim.SetBool("is_rightstep", false);
        }
        //右にステップするとき
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            anim.SetBool("is_rightstep", true);
            rigid.AddForce(this.transform.right * SideForce);
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D))
        {
                anim.SetBool("is_rightstep", false);
        }
        //左連続を防ぐ
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("LeftStep"))
        {
            anim.SetBool("is_leftstep", false);
        }
        //左にステップするとき
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            anim.SetBool("is_leftstep", true);
            rigid.AddForce(-this.transform.right * SideForce);
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A))
        {
            anim.SetBool("is_leftstep", false);
        }
        

    }
}


